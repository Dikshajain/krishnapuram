<?php include_once('header.php') ?>
<?php include_once('latest-news.php') ?>
<div class="content_top clearfix">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12">
<div class="content_left">
<h1 class="blue">Sitemap</h1>
<div class="col-md-4">
<h3 class="green">Pages</h3>
<hr>
<ul class="sitemap">
<li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="index.php">Home</a></li>
<li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="about.php">About Us</a></li>
<li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="krishnapuram.php">Krishnapuram</a></li>
<li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="gallary.php">Current Status</a></li>


<li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="contact.php">Contact Us</a></li>
</div>

<div class="col-md-4">
  <h3 class="green">Others</h3>
  <hr>
  <ul  class="sitemap">

    <li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="privacy-policy.php">Privacy Policy</a></li>
    <li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="terms-conditions.php">Terms & conditions</a></li>

    <li><i class="fa fa-hand-o-right fa-fw green"></i>&nbsp;&nbsp;<a href="disclaimer.php">Disclaimer</a></li>
  </ul>
</div>
</div>
</div>
</div>
</div>
</div>
<?php include_once('footer.php') ?>