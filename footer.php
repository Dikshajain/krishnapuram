<div class="menuFooter clearfix" style="background:#F1F4F7;border-top:5px solid #26333E;">
  <div class="container">
    <div class="row clearfix">
      <div class="col-sm-4 col-xs-6 clearfix">
        <h5 class="text-uppercase">Quick Links</h5>
        <ul class="menuLink clearfix">
          <li><a href="privacy-policy.php">Privacy Policy</a></li>
          <li><a href="terms-conditions.php">Terms & conditions</a></li>
          <li><a href="disclaimer.php">Disclaimer</a></li>
          <li><a href="sitemap.php">Sitemap</a></li>
        </ul>
      </div>
      <div class="col-sm-4 col-xs-6 borderLeft clearfix">
        <div class="footer-address">
          <h5>Site Address:</h5>
          <address>
          <h6 class="green" style="font-size:16px;"><i class="fa fa-building"></i>&nbsp;&nbsp; Krishnapuram</h6>
          Village-Hirri, Tehsil-Bilha, District-Bilaspur, Chhattisgarh-495001, India
          </address>
        </div>
      </div>
      <div class="col-sm-4 col-xs-6 borderLeft clearfix">
        <div class="socialArea clearfix">
          <h5>Follow Us:</h5>
          <ul class="list-inline ">
            <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
          </ul>
        </div>
        <div class="contactNo clearfix">
          <h5>Call Us: <a href="tel:9827123007">+91-9827123007</a>
          <h3></h3>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="footer clearfix">
  <div class="container">
    <div class="row clearfix">
      <div class="col-sm-6 col-xs-12 copyRight">
        <?php //include_once('Counter.php') ?>
      </div>
      <div class="col-sm-6 col-xs-12 privacy_policy"> Developed By : <a href="https://www.spitech.in/" title="SpiTech Web Services Pvt. Ltd." target="_blank"  >SpiTech Web Services Pvt. Ltd.</a> </div>
    </div>
  </div>
</div>
</div>
<script src="js/jquery.min.js"></script> 
<script src="js/bootstrap/bootstrap.min.js"></script> 
<script src="js/plugins/jquery.flexslider.js"></script> 
<script src="js/plugins/jquery.selectbox-0.1.3.min.js"></script> 
<script src="js/plugins/jquery.magnific-popup.js"></script> 
<script src="js/plugins/waypoints.min.js"></script> 
<script src="js/plugins/jquery.counterup.js"></script> 
<script src="js/plugins/wow.min.js"></script> 
<script src="js/plugins/navbar.js"></script> 
<script src="js/plugins/moment.min.js"></script> 
<script src="js/plugins/fullcalendar.min.js"></script> 
<script src="options/optionswitcher.js"></script> 
<script src="js/custom.js"></script>
</body></html>