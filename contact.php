<?php include_once('header.php') ?>
  <script>document.getElementById('contact').setAttribute('class','active')</script>
  <?php include_once('latest-news.php') ?>
  <div class="custom_content custom">
    <div class="container large">
      <div class="row">
        <div class="col-xs-12 col-sm-6 custom_left">
          <div class="sidebar">
            <div class="list_block sidebar_item">
              <h3>Office Address</h3>
              <ul class="contact_info">
                <li><i class="fa fa-map-marker"></i> <strong>Address :</strong>  Village-Hirri, Tehsil-Bilha</li>
                <li><i class="fa fa-fw"></i> Bilaspur, Chhattisgarh-495001, India </li>
                <li><i class="fa fa-phone"></i> <a href="tel:9827123007"><strong>9827123007</strong></a></li>
                <li><i class="fa fa-envelope"></i> <a href="mailto:info@ganeshvalley.com">dinesh-bhootda2000@yahoo.com</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 custom_left">
          <div class="sidebar">
            <div class="list_block sidebar_item">
              <h3>Site Address</h3>
              <ul class="contact_info">
                <li><i class="fa fa-map-marker"></i> <strong>Address :</strong>  Village-Hirri, Tehsil-Bilha</li>
                <li><i class="fa fa-fw"></i> Bilaspur, Chhattisgarh-495001, India </li>
                <li><i class="fa fa-phone"></i> <a href="tel:9827123007"><strong>9827123007</strong></a></li>
               <li><i class="fa fa-envelope"></i> <a href="mailto:info@ganeshvalley.com">dinesh-bhootda2000@yahoo.com</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include_once('footer.php') ?>