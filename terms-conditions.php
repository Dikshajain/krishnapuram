<?php include_once('header.php') ?>
  <?php include_once('latest-news.php') ?>
  <div class="content_top clearfix">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="content_left features">
            <h1 class="blue">Terms &amp; Conditions</h1>
            <p>By accessing this website, you are agreeing to be bound by these website Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trade mark law. </p>
            <p>Permission is granted to temporarily download one copy of the materials (information or software) on  Krishnapuram website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not: </p>
            <p><i class="fa  fa-chevron-circle-right green"></i>&nbsp;  Modify or copy the materials.</p>
            <p><i class="fa  fa-chevron-circle-right green"></i>&nbsp; Use the materials for public display.</p>
            <p><i class="fa  fa-chevron-circle-right green"></i>&nbsp; Attempt to decompile or reverse engineer any software contained on  Krishnapuram website.</p>
            <p><i class="fa  fa-chevron-circle-right green"></i>&nbsp;  Remove any copyright or other proprietary notations from the materials; or</p>
            <p><i class="fa  fa-chevron-circle-right green"></i>&nbsp; Transfer the materials to another person or "mirror" the materials on any other server.</p>
            <p>This license shall automatically terminate if you violate any of these restrictions and may be terminated by  Krishnapuram at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format. </p>
            <p> Krishnapuram may revise these terms of use for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these Terms and Conditions of Use. </p>
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include_once('footer.php') ?>