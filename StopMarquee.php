<?php
function StopStartMarque()
{
   $Browser=GetBrowser();
	if(strtolower($Browser[0])==strtolower('Firefox') || strtolower($Browser[0])==strtolower('msie'))
	{
		echo " onMouseOver='this.setAttribute(`scrollamount`, 0, 0);' OnMouseOut='this.setAttribute(`scrollamount`, 2, 0);' ";
	}
	else
	{
		echo " onmouseover='this.stop()' onmouseout='this.start()' ";
	}
}

function GetBrowser()
{
 $browser = array(
    'version'   => '0.0.0',
    'majorver'  => 0,
    'minorver'  => 0,
    'build'     => 0,
    'name'      => 'unknown',
    'useragent' => ''
  );

  $browsers = array(
    'firefox', 'msie', 'opera', 'chrome', 'safari', 'mozilla', 'safari', 'seamonkey', 'konqueror', 'netscape',
    'gecko', 'navigator', 'mosaic', 'lynx', 'amaya', 'omniweb', 'avant', 'camino', 'flock', 'aol'
  );

  if (isset($_SERVER['HTTP_USER_AGENT'])) 
  {
    $browser['useragent'] = $_SERVER['HTTP_USER_AGENT'];
    $user_agent = strtolower($browser['useragent']);
    foreach($browsers as $_browser) 
	{
      if (preg_match("/($_browser)[\/ ]?([0-9.]*)/", $user_agent, $match)) 
	  {
        $browser['name'] = $match[1];
        $browser['version'] = $match[2];
        $list=@list($browser['majorver'], $browser['minorver'], $browser['build']) = explode('.', $browser['version']);
        break;
      }
    }
  }
  return	array($match[1],$match[2]);

}
?>

