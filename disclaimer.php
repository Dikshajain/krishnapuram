<?php include_once('header.php') ?>
  <?php include_once('latest-news.php') ?>
  <div class="content_top clearfix">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="content_left features">
            <h1 class="blue">Disclaimer &amp; Limitations</h1>
            <h4 class="green">Disclaimer :</h4>
            <p>The materials on  Krishnapuram's website are provided "as is".  Krishnapuram makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further,  Krishnapuram does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet website or otherwise relating to such materials or on any sites linked to this site. </p>
            <h4 class="green">Limitations :</h4>
            <p>In no event shall  Krishnapuram or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on  Krishnapuram's Internet site, even if  Krishnapuram or a  Krishnapuram authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you. </p>
            <p>The materials appearing on  Krishnapuram's website could include technical, typographical, or photographic errors.  Krishnapuram does not warrant that any of the materials on its website are accurate, complete, or current.  Krishnapuram may make changes to the materials contained on its website at any time without notice.  Krishnapuram does not, however, make any commitment to update the materials. </p>
            <p> Krishnapuram has not reviewed all of the sites linked to its Internet website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by  Krishnapuram of the site. Use of any such linked website is at the user's own risk.</p>
          
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include_once('footer.php') ?>