<?php include_once('header.php') ?>
  <script>document.getElementById('about').setAttribute('class','active dropdown')</script>
  <?php include_once('latest-news.php') ?>
  <div class="content_top clearfix">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8">
          <div class="content_left features">
            <h1 class="blue">Vision, Mission & Values</h1>
            <h4 class="green">Vision </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
              
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id</p>
            <h4 class="green">Mission</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
              
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id</p>
            <h4 class="green">Core Values</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
              
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id</p>
          </div>
          <!--end content left--> 
        </div>
        <div class="col-xs-12 col-sm-4">
          <?php include_once('retailer.php') ?>
        </div>
      </div>
      <!--end row--> 
    </div>
    <!--end container--> 
  </div>
  <?php include_once('footer.php') ?>