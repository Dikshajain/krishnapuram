<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Krishnapuram</title>
<meta name="description" content=""/>
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="7 days" />
<meta name="abstract" content="Krishnapuram, Real Estate, Builders in Bilaspur" />
<meta name="copyright" content="Krishnapuram">
<meta name="language" content="en-us">
<meta name="Rating" content="General">
<link rel="shortcut icon" type="image/png" href="images/favicon.png">
<link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
 <link rel="stylesheet" href="css/plugins/select_option1.css"> 
<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/plugins/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/plugins/fullcalendar.min.css">
<link href='css/cyrillic.css' rel='stylesheet' type='text/css'>
<link href='css/roboto.css' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/plugins/animate.css">
<link rel="stylesheet" href="css/plugins/magnific-popup.css"> 
 <link rel="stylesheet" href="css/style.css"> 
<link rel="stylesheet" href="css/colors/default.css" id="option_color">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<style>
  .header navbar-default ul a:hover{
    color:blue;
  }


</style>
<body>
<?php include_once('StopMarquee.php') ?>
<div class="main_wrapper">
<div class="topbar clearfix">
  <div class="container">
    <ul class="topbar-left">
      <li class="mobile-no"><i class="fa fa-phone"></i><a href="tel: 9827123007">9827123007</a></li>
      <li class="email-id"><i class="fa fa-envelope"></i> <a href="dinesh-bhootda2000@yahoo.com">dinesh-bhootda2000@yahoo.com</a> </li>
    </ul>
    <ul class="topbar-right">
      <li class="hidden-xs"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
      <li class="hidden-xs"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
      <li class="hidden-xs"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>

      <li class="hidden-xs"><a href="#" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
    </ul>
  </div>
</div>
<?php include_once('menu.php') ?>
