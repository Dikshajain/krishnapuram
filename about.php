<?php include_once('header.php') ?>
  <script>document.getElementById('about').setAttribute('class','active')</script>
  <?php include_once('latest-news.php') ?>
  <div class="content_top clearfix">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-8">
          <div class="content_left features">
            <h1 class="blue">About Krishnapuram</h1>
            <br>
            <p>Krishnapuram is a residential plot project developed by Krishnapuram. The project offers very well designed Plot. It's a preferred destination for the next generation. The project is well connected by various modes of transportation.<br>
              <br>
              The site is in close proximity to all civic utilities. Our high end specifications and world class amenities stand alone as an answer to the true premium. Every care is taken to mould this sophisticated gated community to a distinguished address. With the enchanting and pleasant lush green garden. </p>
            <p>Krishnapuram is located in Bilha, Bilaspur Chhattisgarh, Offering a range of residential plots on which to develop custom built homes to your taste, style and budget. Located near Mopka road, Grand entrance and a fully secured property. Landscaped streets, Open green areas and wide roads. A large number of colleges and schools including medical and engineering colleges within close proximity. Hospitals, Places of worship, Markets and shopping centers nearby.</p>
          </div>
         
        </div>
        <div class="col-xs-12 col-sm-4"> <img src="images/slider/v1.jpg" alt="About Us" class="img-responsive" width="400px" height="200px"/> </div>
      </div>
    </div>
  </div>
  <?php include_once('footer.php') ?>