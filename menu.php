

<div class="header clearfix">
  <nav class="navbar navbar-main navbar-default">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="header_inner">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand logo clearfix" href="index.php"><img src="images/logo1.png" alt="" class="img-responsive logo-img" /></a> </div>
            <div class="collapse navbar-collapse" id="main-nav">
              <ul class="nav navbar-nav navbar-right">
                <li class="hidden-lg hidden-md hidden-sm"><a>&nbsp;</a></li>
                <li id="home"><a href="index.php">Home</a></li>
                <li id="about"><a href="about.php">About Us</a></li>
                <li id="project"><a href="krishnapuram.php">Krishnapuram</a></li>
                <li id="status" ><a href="gallary.php">Current Status</a></li>
                <li id="contact"><a href="contact.php">Contact Us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </nav>
</div>