<?php include_once('header.php') ?>
  <?php include_once('latest-news.php') ?>
  <div class="content_top clearfix">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="content_left features">
            <h1 class="blue">Privacy Policy</h1>
            <p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.</p>
            <p ><i class="fa  fa-chevron-circle-right green"></i>&nbsp;  Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.
              We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law. </p>
            <p ><i class="fa  fa-chevron-circle-right green"></i>&nbsp; We will only retain personal information as long as necessary for the fulfillment of those purposes.</p>
            <p ><i class="fa  fa-chevron-circle-right green"></i>&nbsp; We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</p>
            <p ><i class="fa  fa-chevron-circle-right green"></i>&nbsp;  Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</p>
            <p ><i class="fa  fa-chevron-circle-right green"></i>&nbsp; We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</p>
            <p ><i class="fa  fa-chevron-circle-right green"></i>&nbsp; We will make readily available to customers information about our policies and practices relating to the management of personal information.</p>
            <p ><i class="fa  fa-chevron-circle-right green"></i>&nbsp; We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. Any claim relating to  Krishnapuram's web site shall be governed by the laws of the State of Chhattisgarh without regard to its conflict of law provisions.</p>
            <p >General Terms and Conditions applicable to Use of a Web Site.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include_once('footer.php') ?>