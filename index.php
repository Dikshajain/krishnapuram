<?php include_once('header.php') ?>
  <script>document.getElementById('home').setAttribute('class','active')</script>
  <?php include_once('slider.php') ?>
  <?php //include_once('latest-news.php') ?>
  <div class="mainContent clearfix">
    <div class="container">
      <div class="row clearfix">
        <div class="col-sm-8 col-xs-12">
          <div class="videoNine clearfix">
            <div class="videoArea clearfix">
              <h3>Welcome to <strong class="green">Krishnapuram</strong></h3>
              <p>Krishnapuram is a residential plot project developed by Krishnapuram. The project offers very well designed Plot. It's a preferred destination for the next generation. The project is well connected by various modes of transportation. The site is in close proximity to all civic utilities.</p>
              <p>Krishnapuram is located in Bilha, Bilaspur Chhattisgarh, Offering a range of residential plots on which to develop custom built homes to your taste, style and budget. Located in Bilha, grand entrance and a fully secured property. Landscaped streets, open green areas and wide roads. A large number of colleges and schools including medical and engineering colleges within close proximity. Hospitals, places of worship, markets and shopping centers nearby. </p>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-xs-12"> <img src="images/content-image.jpg" width="100%" height="100%" alt="Krishnapuram" class="img-responsive"  /> </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="course-grid course-3col">
      <div class="about_inner clearfix">
        <div class="row">
          <div class="col-md-12">
            <h3 class="green">Krishnapuram Views</h3>
            <br>
          </div>
          <div class="col-xs-6 col-sm-4">
            <div class="aboutImage"> <a href="krishnapuram.php"> <img src="images/krishnapuram-entrance-gate.jpg" alt="Krishnapuram" class="img-responsive home-view" />
              <div class="overlay">
                <p class="text-center"><strong>Krishnapuram</strong> is a residential plot project developed by Krishnapuram.</p>
              </div>
              <span class="captionLink">View Details<span></span></span> </a> </div>
          </div>
          <div class="col-xs-6 col-sm-4">
            <div class="aboutImage"> <a href="krishnapuram.php"> <img src="images/krishnapuram-view1.jpg" alt="Krishnapuram" class="img-responsive home-view" />
              <div class="overlay">
                <p class="text-center"><strong>Krishnapuram</strong> is a residential plot project developed by Krishnapuram. </p>
              </div>
              <span class="captionLink">View Details<span></span></span> </a> </div>
          </div>
          <div class="col-xs-6 col-sm-4">
            <div class="aboutImage"> <a href="krishnapuram.php"> <img src="images/krishnapuram-view2.jpg" alt="Krishnapuram" class="img-responsive home-view" />
              <div class="overlay">
                <p class="text-center"><strong>Krishnapuram</strong> is a residential plot project developed by Krishnapuram. </p>
              </div>
              <span class="captionLink">View Details<span></span></span> </a> </div>
      </div>
    </div>
  </div>
</div>
</div>
  <?php include_once('footer.php') ?>